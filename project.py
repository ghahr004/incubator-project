# First we import the modules we need.
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression

source1 = '/Users/peymanghahremani/Dropbox'
source2 = '/Programing and Thesis/incubatorproblem2/honeyproduction.csv'

# We put the data on a data frame.

honey_production = pd.read_csv(source1 + source2)

honey_production.head()

# The following function gives the stock of bees for a given year 
# between 1998 to 2012 and a given state among the ones in the
# dataset.

def state_year_bee_stock(state, year):
    X = honey_production[(honey_production['state'] == str(state)) & (honey_production['year'] == int(year))]
    return int(X['stocks'])

# Using the honey_production defined above we have

all_statess = honey_production['state'].unique() # Which gives all states in the dataframe.

all_states = [str(a) for a in all_statess] # We convert the materials in all_statess into strings.

all_yearss = honey_production['year'].unique()

all_years = [int(a) for a in all_yearss]

# The following function gives the total US bee stocks for a given year
# between 1998 to 2012.

def US_bee_year(year):
    X = honey_production[(honey_production['year'] == int(year))]
    return sum(X['stocks'])


years = [int(a) for a in all_years]
Y = [US_bee_year(a) for a in all_years]

# Above list will sit for the Y-axis of our first graph.

plt.scatter(years, Y,alpha=1)
plt.xlabel('years')
plt.ylabel('Bee Stocks in the USA')
plt.show()

# As the graph shows, the population of bees is a big decrease since 2005.
# Lets fit linear regression method into our data and predict the 
# bee stocks in the US in the year 

model = LinearRegression()

Xprime = np.asarray(years).reshape(-1,1)
Yprime = np.asarray(Y).reshape(-1,1)

model.fit(Xprime,Yprime)

model.predict(2023)

def bee_stock_state(state):
    S = []
    for year in all_years:
        S.append(state_year_bee_stock(state, year))
    return S    


# This function gives the graph of some states which have major bee stocks.
# As it is shows, except HI, the rest of the states are experiencing 
# major decline of bee stocks since 2005.
def scatter_graph(state):
    Y = np.asarray(bee_stock_state(state)).reshape(-1,1)
    plt.scatter(years, Y,alpha=1)
    plt.xlabel('years')
    plt.ylabel('Bee Stocks in' + ' ' + str(state))
    plt.show()















